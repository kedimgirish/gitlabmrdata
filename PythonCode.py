import warnings
warnings.filterwarnings('ignore')

#import ipywidgets as widgets
#from IPython.display import display, clear_output
import matplotlib.pyplot as plt
from datetime import datetime
from dateutil import parser

import gitlab
#git = gitlab.Gitlab("git.censhare.com/")
gl = gitlab.Gitlab("https://git.censhare.com/", "dyNQ_ooaoT378fuRvRe8")
project = gl.projects.get(276, lazy=True)
openMrs = project.mergerequests.list(all=True, state='merged')
print(len(openMrs))
for mr in openMrs:
    print(len(mr.notes.list()))


projectSelected = widgets.Dropdown(
options = [ (gl.projects.get(276).name,276),
            (gl.projects.get(209).name,209)],
            description='Select the Project:',)
w.observe(on_change,names='value')
display(projectSelected)

def on_change(change):
    project = gl.projects.get(change['new'])
    openMrs = project.mergerequests.list(all=True, state='merged')
    MrCreated = []
    for openMr in openMrs:
        formattedDate = parser.isoparse(openMr.attributes['created_at'])
        MrCreated.append(datetime(formattedDate.year, formattedDate.month, formattedDate.day))
    plt.figure(figsize=(16,8))
    plt.hist(MrCreated)
